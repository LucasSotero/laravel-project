@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2 class="text-center" >Clientes</h2>
        </div>
        <div class="pull-right">
            <a href="{{route('clients.create')}}" class="btn btn-primary">Novo Cliente</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success" role="alert">
        <h5>{{ $message }}</h5>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>Codigo</th>
        <th>Nome</th>
        <th width="280px" class="text-center">Ações</th>
    </tr>
@foreach ($clients as $client)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $client->name }}</td>
        <td>
            <form action="{{ route('clients.destroy', $client->id) }}" method="POST">
                <a href="{{ route('clients.show', $client->id) }}" class="btn btn-info">Destalhes</a>
                <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-primary">Editar</a>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Deletar</button>
            </form>
        </td>
    </tr>
@endforeach
</table>

<div class="text-center">{!! $clients->links() !!}</div>
</div>
@endsection
